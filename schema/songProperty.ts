import { list } from "@keystone-6/core";
import { relationship, text, integer, float } from "@keystone-6/core/fields";
import { isAdmin, isVisitor, visitorRole } from "./accessControl";

export const SongProperty = list({
  fields: {
    name: text({ validation: { isRequired: true } }),
    song: relationship({
      ref: "Song.songProperty",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
      },
    }),
    mainCategory: relationship({
      ref: "MainCategory.songProperty",
      many: false,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
      },
    }),
  },
  ui: {
    listView: {
      initialColumns: ["name"],
    },
  },
  access: {
    operation: {
      query: isVisitor,
      create: isAdmin,
      update: isAdmin,
      delete: isAdmin,
    },
  },
});
