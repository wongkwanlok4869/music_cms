import { list } from "@keystone-6/core";
import { isAdmin, isVisitor, visitorRole } from "./accessControl";
import {
  text,
  file,
  virtual,
  select,
  checkbox,
  calendarDay,
  integer,
  relationship,
} from "@keystone-6/core/fields";
import moment, { months } from "moment";
export const Song = list({
  fields: {
    name: text({ validation: { isRequired: true } }),
    songProperty: relationship({
      ref: "SongProperty.song",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
        // views: "difficulty",
      },
    }),
    composer: relationship({
      ref: "Composer.song",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
        // views: "difficulty",
      },
    }),
    lyricist: relationship({
      ref: "Lyricist.song",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
        // views: "difficulty",
      },
    }),
    pdf: file({
      storage: "my_file_storage",
    }),
    introduction: text({}),
    youtubeUrl: text({}),
  },
  ui: {
    listView: {
      initialColumns: ["name"],
    },
    labelField: "name",
  },

  access: {
    operation: {
      query: isVisitor,
      create: isAdmin,
      update: isAdmin,
      delete: isAdmin,
    },
  },
});
const format = (input: number, padLength: number): string => {
  return input.toString().padStart(padLength, "0");
};
