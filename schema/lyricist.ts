import { list } from "@keystone-6/core";
import {
  relationship,
  text,
  integer,
  float,
  image,
} from "@keystone-6/core/fields";
import { isAdmin, isVisitor, visitorRole } from "./accessControl";

export const Lyricist = list({
  fields: {
    name: text({}),
    song: relationship({
      ref: "Song.lyricist",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
      },
    }),
    img: image({ storage: "my_image_storage" }),
  },
  ui: {
    listView: {
      initialColumns: ["name"],
    },
  },
  access: {
    operation: {
      query: isVisitor,
      create: isAdmin,
      update: isAdmin,
      delete: isAdmin,
    },
  },
});
