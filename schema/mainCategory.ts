import { list } from "@keystone-6/core";
import { relationship, text, integer, float } from "@keystone-6/core/fields";
import { isAdmin, isVisitor, visitorRole } from "./accessControl";

export const MainCategory = list({
  fields: {
    name: text({ validation: { isRequired: true } }),
    songProperty: relationship({
      ref: "SongProperty.mainCategory",
      many: true,
      isFilterable: true,
      ui: {
        hideCreate: false,
        linkToItem: true,
        inlineConnect: true,
      },
    }),
  },
  ui: {
    listView: {
      initialColumns: ["name"],
    },
  },
  access: {
    operation: {
      query: isVisitor,
      create: isAdmin,
      update: isAdmin,
      delete: isAdmin,
    },
  },
});
