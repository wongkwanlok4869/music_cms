export const adminRole = "admin";
export const visitorRole = "visitor";

export const isAdmin = ({ context }: any) => {
  console.log(context.session?.data?.role);
  return [adminRole].includes(context.session?.data?.role);
};
export const isNotAdmin = ({ context }: any) => {
  return ![adminRole].includes(context.session?.data?.role);
};

export const isVisitor = ({ context }: any) => {
  return [adminRole, visitorRole].includes(context.session?.data?.role);
};

export const isNotVisitor = ({ context }: any) => {
  return ![adminRole, visitorRole].includes(context.session?.data?.role);
};
