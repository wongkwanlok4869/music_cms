/*
Welcome to Keystone! This file is what keystone uses to start the app.

It looks at the default export, and expects a Keystone config object.

You can find all the config options in our docs here: https://keystonejs.com/docs/apis/config
*/

import { config } from "@keystone-6/core";

// Look in the schema file for how we define our lists, and how users interact with them through graphql or the Admin UI
import { lists } from "./schema";

// Keystone auth is configured separately - check out the basic auth setup we are importing from our auth file.
import { withAuth, session } from "./auth";
const fs = require("fs");
const path = require("path");
const { PDFDocument, rgb } = require("pdf-lib");
function extractPathFromUrl(urlString: string) {
  const url = new URL(urlString);
  return url.pathname;
}
function getCurrentDateFormatted() {
  const date = new Date();
  const day = String(date.getDate()).padStart(2, "0");
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const year = String(date.getFullYear());
  return `${day}-${month}-${year}`;
}
function deleteModifiedFiles() {
  console.log(process.env.GET_FILE_URL);
  const directoryPath = "./public/file/";

  fs.readdir(directoryPath, (err: any, files: any) => {
    if (err) {
      console.error(`Error reading directory: ${err}`);
      return;
    }

    const now = new Date();
    const tenMinutesAgo = now.getTime() - 0.1 * 60 * 1000; // 10 minutes ago
    files.forEach((file: any) => {
      const filePath = path.join(directoryPath, file);
      const stats = fs.statSync(filePath);
      const fileAge = (now.getTime() - stats.birthtime) / 60000; // in milliseconds
      console.log(fileAge);
      if (file.includes("modified") && fileAge > 15) {
        console.log(`Deleting ${file}`);
        fs.unlinkSync(filePath);
      }
    });
  });
}

setInterval(deleteModifiedFiles, 0.1 * 60 * 1000); // run every 10 minutes

export default withAuth(
  // Using the config function helps typescript guide you to the available options.
  config({
    server: {
      cors: {
        credentials: true,
        origin: process.env.ALLOWED_ORIGINS,
      },
      maxFileSize: 9999 * 1024 * 1024,
      extendExpressApp: (app, createContext) => {
        // use helmet for security!
        app.get("/get_pdf", async (req, res: any) => {
          const filePath = req.query.filePath
            ? req.query.filePath
            : "http://localhost:3000/file/ae-a-ae-7-full-score-3-1-GRAcQs0HMLW7uHZPtrjQ.pdf";
          const schoolName = req.query.schoolName ? req.query.schoolName : "";
          const responseFilePath =
            "public" + extractPathFromUrl(filePath.toString());
          if (fs.existsSync(responseFilePath) || schoolName != "") {
            const pdfFile = fs.readFileSync(responseFilePath);
            const pdfDoc = await PDFDocument.load(pdfFile);
            const font = await pdfDoc.embedFont("Helvetica-Bold");
            const pageCount = pdfDoc.getPageCount();
            for (let i = 0; i < pageCount; i++) {
              const firstPage = pdfDoc.getPages()[i];
              const { width, height } = firstPage.getSize();
              const redColor = rgb(1, 0, 0);
              const text =
                "Licensed to " +
                schoolName +
                " by Hong Kong interschool choral festival on " +
                getCurrentDateFormatted();
              const fontSize = 12;
              const textWidth = font.widthOfTextAtSize(text, fontSize);
              const textHeight = font.heightAtSize(fontSize);
              const x = (width - textWidth) / 2;
              const y = textHeight + 10; // 10 is the margin from the bottom
              firstPage.drawText(text, {
                x,
                y,
                size: fontSize,
                font,
                color: redColor,
              });
            }
            const modifiedPdfFile = await pdfDoc.save();
            const modifiedPath = schoolName.toString() + "_modified_file.pdf";
            fs.writeFileSync("./public/file/" + modifiedPath, modifiedPdfFile);
            res.send(process.env.GET_FILE_URL + "/file/" + modifiedPath);
          } else {
            res.send("error");
          }
        });
      },
    },
    storage: {
      my_file_storage: {
        kind: "local",
        type: "file",
        generateUrl: (path) => `${process.env.GET_FILE_URL}/file${path}`,
        serverRoute: {
          path: "/file",
        },
        storagePath: "public/file",
      },
      my_image_storage: {
        kind: "local",
        type: "image",
        generateUrl: (path) => `${process.env.GET_FILE_URL}/image${path}`,
        serverRoute: {
          path: "/image",
        },
        storagePath: "public/image",
      },
    },
    // the db sets the database provider - we're using sqlite for the fastest startup experience
    db: {
      provider: "sqlite",
      url: "file:./keystone.db",
    },

    // This config allows us to set up features of the Admin UI https://keystonejs.com/docs/apis/config#ui
    ui: {
      // For our starter, we check that someone has session data before letting them see the Admin UI.
      isAccessAllowed: (context) => !!context.session?.data,
    },
    lists,
    session,
  })
);
